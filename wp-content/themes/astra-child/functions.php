<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );



add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

add_action( 'wp_enqueue_scripts', function(){

	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/base.min.css", array(), "", false);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
});

function current_year(){
   return date('Y');
}

add_shortcode('fl_year','current_year');

add_filter( 'facetwp_template_html', function( $output, $class ) {
    //$GLOBALS['wp_query'] = $class->query;
    $prod_list = $class->query;  
    ob_start();       
   // write_log($class->query_args['check_instock']);
    if(isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1){

        $dir = get_stylesheet_directory().'/product-instock-loop-great-floors.php';

    }else{

        $dir = get_stylesheet_directory().'/product-loop-great-floors.php';
    }
   
    require_once $dir;    
    return ob_get_clean();
}, 10, 2 );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

add_filter("wpseo_breadcrumb_links", "override_yoast_breadcrumb_pdp_page");

function override_yoast_breadcrumb_pdp_page($links){
    $post_type = get_post_type();

    if ($post_type === 'instock_luxury_vinyl') {
		$breadcrumb[] = array(
            'url' => get_site_url().'/in-stock-products/',
            'text' => 'In Stock Flooring',
        );
        $breadcrumb[] = array(
            'url' => '/in-stock-products/luxury-vinyl-tile/',
            'text' => 'In-Stock Luxury Vinyl Tile'
        );
        array_splice($links, 1, -3, $breadcrumb);
    }
      else if ($post_type === 'instock_laminate') {
		  $breadcrumb[] = array(
            'url' => get_site_url().'/in-stock-products/',
            'text' => 'In Stock Flooring',
        );
        $breadcrumb[] = array(
            'url' => '/in-stock-products/laminate/',
            'text' => 'In-Stock Laminate Flooring'
        );
        array_splice($links, 1, -3, $breadcrumb);
    }
    
    else if ($post_type === 'instock_carpet') {
		 $breadcrumb[] = array(
            'url' => get_site_url().'/in-stock-products/',
            'text' => 'In Stock Flooring',
        );
        $breadcrumb[] = array(
            'url' => '/in-stock-products/carpet/',
            'text' => 'In-Stock Carpet'
        );
        array_splice($links, 1, -3, $breadcrumb);
    }
	 //return $links;
    return $links;

    
}